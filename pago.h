#ifndef PAGO_H
#define PAGO_H

#include <QObject>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

class Pago : public QObject
{
    Q_OBJECT

    QString token;
    QString tokenChofer;
    double monto;

public:
    explicit Pago(QObject *parent = nullptr);

    QString getToken() const;
    void setToken(const QString &value);

    QString getTokenChofer() const;
    void setTokenChofer(const QString &value);

    double getMonto() const;
    void setMonto(double value);

    Q_INVOKABLE QByteArray crearJSON(QString token);

signals:

public slots:
};

#endif // PAGO_H
