#include "pago.h"

QString Pago::getToken() const
{
    return token;
}

void Pago::setToken(const QString &value)
{
    token = value;
}

QString Pago::getTokenChofer() const
{
    return tokenChofer;
}

void Pago::setTokenChofer(const QString &value)
{
    tokenChofer = value;
}

double Pago::getMonto() const
{
    return monto;
}

void Pago::setMonto(double value)
{
    monto = value;
}

QByteArray Pago::crearJSON(QString token)
{
    QByteArray arreglo;

    QJsonObject perfil;
    perfil["driver"] = 123123;
    perfil["passenger"] = token;
    perfil["amount"] = 175;

    QJsonDocument documento(perfil);

    arreglo.append(documento.toJson(QJsonDocument::Compact));
    return arreglo;
}

Pago::Pago(QObject *parent) : QObject(parent)
{

}
