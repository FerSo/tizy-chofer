#include "usuario.h"

QString Usuario::getNombre() const
{
    return nombre;
}

void Usuario::setNombre(const QString &value)
{
    nombre = value;
}

QString Usuario::getContrasena() const
{
    return contrasena;
}

void Usuario::setContrasena(const QString &value)
{
    contrasena = value;
}

QString Usuario::getCedula() const
{
    return cedula;
}

void Usuario::setCedula(const QString &value)
{
    cedula = value;
}

QString Usuario::getCorreo() const
{
    return correo;
}

void Usuario::setCorreo(const QString &value)
{
    correo = value;
}

QString Usuario::getTelefono() const
{
    return telefono;
}

void Usuario::setTelefono(const QString &value)
{
    telefono = value;
}

int Usuario::getGenero() const
{
    return genero;
}

void Usuario::setGenero(const int &value)
{
    genero = value;
}

void Usuario::parsearJSON(QJsonObject respuesta)
{
    QJsonObject data = respuesta.value("data").toObject();

    QJsonObject dataUser = data.value("dataUser").toObject();

    nombre = dataUser.value("name").toString();
    cedula = dataUser.value("dni").toString();
    correo = dataUser.value("email").toString();
    telefono = dataUser.value("phone").toString();
    contrasena = dataUser.value("password").toString();
    token = dataUser.value("token").toString();
    genero = dataUser.value("gender").toInt();
}

QByteArray Usuario::crearJSONLogin( )
{
    QByteArray arreglo;

    QJsonObject perfil;
    perfil["dni"] = cedula;
    perfil["password"] = contrasena;

    QJsonDocument documento(perfil);

    arreglo.append(documento.toJson(QJsonDocument::Compact));
    return arreglo;
}

QByteArray Usuario::crearJSONRegistro()
{
    QByteArray arreglo;

    QJsonObject perfil;
    perfil["name"] = nombre;
    perfil["dni"] = cedula;
    perfil["email"] = correo;
    perfil["phone"] = telefono;
    perfil["password"] = contrasena;
    perfil["gender"] = genero;

    QJsonDocument documento(perfil);

    arreglo.append(documento.toJson(QJsonDocument::Compact));
    return arreglo;
}

void Usuario::parsearSaldo(QJsonObject respuesta)
{
    saldo = respuesta.value("data").toDouble();
}

QString Usuario::getToken() const
{
    return token;
}

void Usuario::setToken(const QString &value)
{
    token = value;
}

double Usuario::getSaldo() const
{
    return saldo;
}

void Usuario::setSaldo(double value)
{
    saldo = value;
}

Usuario::Usuario(QObject *parent) : QObject(parent)
{

}
