import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.1

import Material 0.3
import Material.ListItems 0.1 as ListItem

import "qrc:/controladores/ControladorLector.js" as Controlador

Page
{
    id: procesando
    width: parent.width
    height: parent.height
    actionBar.hidden: true

    Connections
    {
        target: restPago
        onCambioEstatusRespuesta: Controlador.recibirRespuestaPago()
    }

    Text
    {
        text: "Procesando"
        font.pointSize: 30
        anchors.centerIn: parent
    }


}
