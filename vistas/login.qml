import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

import Material 0.3
import Material.ListItems 0.1 as ListItem

Page
{
    id: login
    actionBar.hidden: true
    width: Screen.width
    height: Screen.height

    Image
    {
        id: logo
        source: "qrc:/imagenes/logo"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: formularioLogin.top
        anchors.bottomMargin: 20
        width: parent.width*0.2
        height: width
        z: 2
    }

    Item
    {
        id: formularioLogin
        width: parent.width*0.8
        height: parent.height*0.4
        anchors.centerIn: parent
        z:2

        ColumnLayout
        {
            width: parent.width
            height: parent.height
            anchors.centerIn: parent

            TextField
            {
                id: usuario
                implicitWidth:  parent.width*0.7
                Layout.alignment: Qt.AlignHCenter
                placeholderText: qsTr("Usuario")
                floatingLabel: true
            }

            TextField
            {
                id: contrasena
                implicitWidth:  parent.width*0.7
                Layout.alignment: Qt.AlignHCenter
                placeholderText: qsTr("Contrasena")
                echoMode: TextInput.Password
                floatingLabel: true
            }

            Button
            {
                id:botonIniciar
                text: qsTr("Iniciar")
                Layout.alignment: Qt.AlignHCenter
                backgroundColor: tema.primaryColor
                onClicked: pageStack.push("qrc:/perfil");
            }
        }

    }



    Image
    {
        id: fondo
        source: "qrc:/imagenes/fondo"
    }

}
