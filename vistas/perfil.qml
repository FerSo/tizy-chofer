import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

import Material 0.3
import Material.ListItems 0.1 as ListItem

Page
{
    id: perfil
    width: parent.width
    height: parent.height

    title: qsTr("Perfil")

    ActionBar
    {
        customContent: Text {
            id: titulo
            text: qsTr("Perfil")
            color: tema.backgroundColor
            anchors.centerIn: parent
        }
    }

    backAction: menu.action

    NavigationDrawer
    {
        id: menu
    }
}

