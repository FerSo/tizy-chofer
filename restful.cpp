#include "restful.h"

Rest *Restful::getEndpointPago() const
{
    return endpointPago;
}

void Restful::setEndpointPago(Rest *value)
{
    endpointPago = value;
}

Restful::Restful(QObject *parent) : QObject(parent)
{
    endpointPago = new Rest(this);

}
